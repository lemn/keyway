# Keyway

Keyway is a minimal, proof-of-concept, user authentication system that uses easily rotated pre-shared auth secrets and "mixers" as an additional client validation. These are packed into a WASM client to further obscure the API behavior.

## Getting Started

You'll need to install the `sqlx` cli before starting to setup your database: `cargo install sqlx-cli`. Next c)reate a `.env` file which includes the line `export DATABASE_URL=postgres://...` pointing to your database. The included `run_postgres.sql` script will launch a local postgres docker container with user `postgres` and the env var `$DB_PASSWORD` for the password. Once your database is up, run `sqlx database setup` to setup the schema.

Now just do `source .env; cd server; RUST_LOG=info cargo run` and you should be up and running.

## Usage

Go ahead and register a new user using curl:

    curl localhost:3000/register -v -H 'Content-Type: application/json' -d '{"username":"my_login","password":"my_password", "v1": 1229604374, "v2": [43,128,135,253,201,238,183,246,25,188,95,224,74,11,171,68,46,234,157,80,211,216,36,110]}'

    *   Trying 127.0.0.1:3000...
    * Connected to localhost (127.0.0.1) port 3000 (#0)
    > POST /register HTTP/1.1
    > Host: localhost:3000
    > User-Agent: curl/7.81.0
    > Accept: */*
    > Content-Type: application/json
    > Content-Length: 79
    >
    * Mark bundle as not supporting multiuse
    < HTTP/1.1 201 Created
    < content-type: application/json
    < content-length: 85
    < date: Sat, 08 Oct 2022 16:43:31 GMT
    <
    * Connection #0 to host localhost left intact
    {"id":1,"username":"my_login","password":"my_password"}

Now let's login:

    curl localhost:3000/login -H 'Content-Type: application/json' -v -d '{"username": "my_login", "password": "my_password", "v1": 1229604398, "v2": [16,47,140,33,130,201,110,24,210,155,154,236,150,82,145,223,124,67,132,244,32,48,246,66]}'

    *   Trying 127.0.0.1:3000...
    * Connected to localhost (127.0.0.1) port 3000 (#0)
    > POST /login HTTP/1.1
    > Host: localhost:3000
    > User-Agent: curl/7.81.0
    > Accept: */*
    > Content-Type: application/json
    > Content-Length: 79
    >
    * Mark bundle as not supporting multiuse
    < HTTP/1.1 200 OK
    < content-type: application/json
    < content-length: 85
    < date: Sat, 08 Oct 2022 16:45:23 GMT
    <
    * Connection #0 to host localhost left intact
    {"id":2,"username":"my_login","password":"my_password"}

Here we see the inclusion of the values `v1` and `v2` in the login request. The value `v1` is a time-based token provided by the client and validated on the server (client time must be +/- 300s of server time), while the value `v2` is computed by combining `v1` with a pre-shared secret and the `create_signature()` function. If the value provided for `v2` does not match what the server calculates during the authorization attempt, the login will fail with a 403 return.

