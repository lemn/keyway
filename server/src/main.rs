use std::time::SystemTime;
use axum::{
    extract::State,
    http::StatusCode,
    routing::post,
    Json, Router,
};
use tower_http::cors::CorsLayer;
use sqlx::{postgres::PgPoolOptions, PgPool};
use serde::{Deserialize, Serialize};

use std::net::SocketAddr;
use std::env;

use keyway_lib::*;

#[tokio::main]
async fn main() {
    tracing_subscriber::fmt::init();

    let db = Database::new(env::var("DATABASE_URL")
        .expect("environment variable DATABASE_URL must be set"))
        .await
        .expect("failed to connect to database");
    tracing::debug!("connected to postgres database");

    let app = Router::with_state(db)
        .route("/register", post(register))
        .route("/login", post(login))
        .layer(CorsLayer::permissive());

    let addr = SocketAddr::from(([0, 0, 0, 0], 3000));
    tracing::debug!("listening on {}", addr);
    axum::Server::bind(&addr)
        .serve(app.into_make_service())
        .await
        .expect("server error");
}

#[derive(Clone)]
pub struct Database {
    pool: PgPool
}
 
impl Database {
    pub async fn new(config: String) -> Result<Database, sqlx::Error> {
        let pool = PgPoolOptions::new()
            .max_connections(5)
            .connect(&config)
            .await?;
        Ok(Database { pool })
    }

    async fn create_user(&self, user: NewUser) -> Result<User, sqlx::Error> {
        tracing::debug!("adding new user {} to db,", user.username);
        let mut transaction = self
            .pool
            .begin()
            .await?;
        let user = sqlx::query_as("INSERT INTO users(username, password) VALUES ($1, $2) RETURNING *;")
            .bind(user.username)
            .bind(user.password)
            .fetch_one(&mut transaction).await?;
        transaction.commit().await?;
        Ok(user)
    }

    async fn get_user(&self, username: &str) -> Result<User, sqlx::Error> {
        tracing::debug!("getting user {} from db", username);
        let mut transaction = self
            .pool
            .begin()
            .await?;
        let user: User = sqlx::query_as("SELECT * FROM users where username = $1;")
            .bind(username)
            .fetch_one(&mut transaction).await?;
        transaction.commit().await?;
        Ok(user)
    }

}

async fn register(State(db): State<Database>, Json(payload): Json<RegisterReq>) -> (StatusCode, Json<RegisterRes>) {
    // return 403 + empty user if username already exists
    let existing = db.get_user(&payload.username).await.is_ok();
    if existing {
        tracing::info!("user {} already exists", payload.username);
        return (StatusCode::FORBIDDEN, (Json(RegisterRes{result: String::from("user already exists"), token: String::from("")})))
    }
    if !check_v1(payload.v1) {
        tracing::info!("registration for {} failed (v1 check failed)", payload.username);
        return (StatusCode::FORBIDDEN, (Json(RegisterRes{result: String::from("v1 check failed"), token: String::from("")})))
    }
    let sig = create_signature(payload.v1, SECRET);
    if sig != payload.v2 {
        tracing::info!("registration for {} failed (signature check failed)", payload.username);
        return (StatusCode::FORBIDDEN, (Json(RegisterRes{result: String::from("signature check failed"), token: String::from("")})))
    }
    let user = NewUser {
        username: payload.username,
        password: payload.password
    };
    let user = db.create_user(user).await.expect("create user failed");
    tracing::info!("user {} created", user.username);
    (StatusCode::CREATED, (Json(RegisterRes{result: String::from("success"), token: String::from("0123456789012345")})))
}

async fn login(State(db): State<Database>, Json(payload): Json<LoginReq>) -> (StatusCode, Json<LoginRes>) {
    let user = db.get_user(&payload.username).await;
    match user {
        Ok(user) => {
            let LoginReq {v1, v2, password, ..} = payload;
            if !check_v1(payload.v1) {
                tracing::info!("login for {} failed (v1 check failed)", user.username);
                return (StatusCode::FORBIDDEN,(Json(LoginRes{result: String::from(""), token: String::from("")})))
            }
            if !user.auth(v1, v2, &password) {
                tracing::info!("login for {} failed (signature check failed)", user.username);
                return (StatusCode::FORBIDDEN, (Json(LoginRes{result: String::from(""), token: String::from("")})))
            }
                tracing::info!("login for {} succeeded", user.username);
                (StatusCode::OK, (Json(LoginRes{result: String::from("success"), token: String::from("0123456789012345")})))
        }
        Err(_) => {
            tracing::debug!("requested user {} does not exist", payload.username);
            (StatusCode::FORBIDDEN, (Json(LoginRes{result: String::from("requested user does not exist"), token: String::from("")})))
        }
    }
}


#[derive(sqlx::FromRow, Deserialize, Serialize, Default)]
struct User {
    id: i64,
    username: String,
    password: String
}

struct NewUser {
    username: String,
    password: String
}

impl User {
    pub fn auth(&self, v1: u64, v2: [u8; 24], password: &str) -> bool {
        self.password == password && create_signature(v1, SECRET) == v2
    }
}

fn check_v1(v1: u64) -> bool {
    let lower = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs() - OFFSET - 300 ;
    let upper = lower + 600;
    v1 >= lower && v1 <= upper 
}
