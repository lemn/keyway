use serde::{Serialize, Deserialize};
use tiger::{Tiger, Digest};

pub const SECRET: u64 = 9001;
pub const OFFSET: u64 = 1672;

const S1: u64 = 0;
const S2: u64 = 1;
const S3: u64 = 2; // # of hash rounds, don't set too high

pub fn create_signature(salt: u64, secret: u64)  -> [u8; 24] { 
    // do some xor with S1 and S2 to allow for easy rotations
    let secret = secret ^ S1;
    let salt = salt ^ S2;
    let secret = secret ^ salt;

    let mut hash = Tiger::digest(secret.to_be_bytes());
    let mut n = S3;
    while n > 0 {
        n -= 1;
        hash = Tiger::digest(hash);
    }
    let mut output: [u8; 24] = [0; 24];
    output.copy_from_slice(&hash);
    output
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct LoginReq {
    pub username: String,
    pub password: String,
    pub v1: u64,
    pub v2: [u8; 24],
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct RegisterReq {
    pub username: String,
    pub password: String,
    pub v1: u64,
    pub v2: [u8; 24], 
}

#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct LoginRes {
    pub result: String,
    pub token: String
}


#[derive(Serialize, Deserialize, PartialEq, Eq, Clone)]
pub struct RegisterRes {
    pub result: String,
    pub token: String
}
