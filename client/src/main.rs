use yew::prelude::*;
use wasm_timer::SystemTime;
use gloo_net::http::Request;
use gloo::console::log;
use keyway_lib::*;
use std::ops::Deref;

mod text_input;

const SERVER_IP: &str = "20.237.143.102";

#[derive(PartialEq, Eq, Clone)]
pub enum Res {
    Login(bool),
    Register(bool)
}

#[derive(Default, Clone)]
pub struct UserData {
    pub username: String,
    pub password: String
}

#[function_component(Form)]
fn form() -> Html { 
    let userdata: UserData = Default::default();
    let state = use_state(|| userdata);
    let call_result:yew::UseStateHandle<Option<Res>> = use_state(|| None);

    let cloned_state = state.clone();
    let username_changed = Callback::from(move |username: String| {
        let mut data = cloned_state.deref().clone();
        data.username = username;
        cloned_state.set(data);
    });

    let cloned_state = state.clone(); 
    let password_changed = Callback::from(move |password: String| {
        let mut data = cloned_state.deref().clone();
        data.password = password;
        cloned_state.set(data);
    });

    let cloned_state = state.clone(); 
    let cloned_result = call_result.clone();
    let do_login = Callback::from(move |event: MouseEvent| {
        event.prevent_default();
        let data = cloned_state.deref().clone();
        log!("attempting to login user...");
        let v1 = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs() - OFFSET;
        let req = LoginReq{
                username: data.username,
                password: data.password,
                v1, v2: create_signature(v1, SECRET),
            };
        let res = cloned_result.clone();
        wasm_bindgen_futures::spawn_local(async move {
            let resp = Request::post(&format!("http://{}:3000/login", SERVER_IP))
                .json(&req)
                .expect("invalid request json")
                .send()
                .await
                .expect("login network request failed");
            if resp.status() == 200 {
                res.set(Some(Res::Login(true)));
                log!("login successful")
            } else {
                res.set(Some(Res::Login(false)));
                log!("login failed")
            }
        });
    });

    let cloned_result = call_result.clone();
    let do_register = Callback::from(move |event: MouseEvent| {
        event.prevent_default();
        let data = state.deref().clone();
        log!("attempting to register user...");
        let v1 = SystemTime::now().duration_since(SystemTime::UNIX_EPOCH).unwrap().as_secs() - OFFSET;
        let req = RegisterReq{
                username: data.username,
                password: data.password,
                v1, v2: create_signature(v1, SECRET)
            };
        let res = cloned_result.clone();
        wasm_bindgen_futures::spawn_local(async move {
            let resp = Request::post(&format!("http://{}:3000/register", SERVER_IP))
                .json(&req)
                .expect("invalid request json")
                .send()
                .await
                .expect("registration network request failed");
            if resp.status() == 201 {
                res.set(Some(Res::Register(true)));
                log!("registration successful")
            } else {
                res.set(Some(Res::Register(false)));
                log!("registration failed")
            }
        });
    });

    html! {
        <div class="login-form">
        <p align="center">{"This is a proof-of-concept for API pre-authentication."}</p>
        <p align="center">{"Data is not secured."}</p>
        <p align="center">{"Please do not use any real passwords for testing."}</p>
            <form>
            <h1>{"Keyway Login"}</h1>
            <div class="content">
                <text_input::TextInput name="username" handle_onchange={username_changed} />
                <text_input::TextInput name="password" handle_onchange={password_changed} />
                <CallResult res={ call_result.deref().clone() }  />
            </div>
            <div class="action">
                <button onclick={do_register}>{"Register"}</button>
                <button onclick={do_login}>{"Sign in"}</button>
            </div>
            </form>
        </div>
    }
}

#[derive(Clone, Properties, PartialEq)]
struct CallResultProps {
    res: Option<Res>
}

#[function_component(CallResult)]
fn call_result(CallResultProps { res }: &CallResultProps) -> Html {
    match res {
        Some(res) => match res {
            Res::Login(res) => {
                html! { {format!("Login {}", if *res {"succeeded"} else {"failed"} )} }
            },
            Res::Register(res) => {
                html! { {format!("Registration {}", if *res {"succeeded"} else {"failed"} )} }
            }
        },
        None => html! {""}
    }
}

#[function_component(App)]
fn app() -> Html {
    html! {
        <Form />
    } 
}

fn main() {
    yew::start_app::<App>();
    log!("keyway client started");
}
