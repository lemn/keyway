docker run \
  -e POSTGRES_PASSWORD=$DB_PASSWORD \
  -e PGDATA=/var/lib/postgresql/data/pgdata \
  -v $PWD/db:/var/lib/postgresql/data/pgdata \
  -p 5432:5432 \
  -d postgres
